import React from 'react';
import Contador from './components/Contador';
import NoticiasSaga from './components/NoticiasSaga';
import Loading from './components/Loading';
import CambiarEstado from './components/CambiarEstado';

function App() {
  return (
    <div>
      <Contador />
      <Loading />
      <NoticiasSaga />
      <CambiarEstado />
    </div>
  );
}

export default App;
