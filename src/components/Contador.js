import React from 'react';
import { useDispatch, useSelector } from "react-redux";
import { addOne, minusOne } from '../actions';
import './style.css';

const Contador = () => {
    const contador = useSelector(state => state.count)
    const dispatch = useDispatch();

    return (
        <div>
            <h1>Contador: Redux con Hooks</h1>
            <div className="counter">
                contador: {contador}
            </div>
            <button onClick={()=> dispatch(addOne())}>
                Aumentar
            </button>

            <button onClick={()=> dispatch(minusOne)}>
                Restar
            </button>
            <hr/>
        </div>
    );
};

export default Contador;