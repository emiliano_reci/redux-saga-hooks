import React from 'react';
import { useSelector, shallowEqual } from 'react-redux';
import reloj from '../images/loading_spinner.gif';
import './style.css';   

const Loading = () => {
    const cargando = useSelector(state => state.loading, shallowEqual)
    return (
        <div>
            { cargando ?
                <div className="loading"> 
                    <img src={reloj} alt="cargando..." />
                </div>
                : null
            }
        </div>
    );
};

export default Loading;