import React, { Component } from 'react';

class CambiarEstado extends Component {
    state={
        age:20
    }

    restar = async() =>{
        console.log(this.state.age)
        await this.setState({
            age: this.state.age -10
        })
        console.log(this.state.age)
        let ar = [this]
        console.log(ar)
    }
    render() {
        return (
            <div>
                edad: {this.state.age}
                <button onClick={this.restar}>
                    Menos 10
                </button>
            </div>
        );
    }
}

export default CambiarEstado;