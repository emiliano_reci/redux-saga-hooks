import React from 'react';
import { useDispatch, useSelector, shallowEqual } from "react-redux";
import { getUser } from '../actions';

const NoticiasSaga = () => {
    const noticias = useSelector(state => state.news, shallowEqual)
    const dispatch = useDispatch();
    return (
        <div>
            <h1>Contador: Hooks con Saga</h1>
            <button onClick={()=> dispatch(getUser)}>
                Traer Noticias
            </button>
            { noticias ? 
                <ul>
                    {noticias.map(eachElement => <li>
                        <div>
                            <h4>{eachElement.title}</h4>
                            <a href={eachElement.url} target="_blank" rel='noreferrer noopener'>
                                <img src={eachElement.urlToImage} alt="banana" className="img_art" />
                            </a>
                        </div>
                    </li> )}
                </ul> 
                : ' Sin Noticias'
            }
            <hr/>
        </div>
    );
};

export default NoticiasSaga;