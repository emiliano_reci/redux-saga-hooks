const addOne = ()=> ({
    type: "ADD_ONE_TO_COUNT"
});

const minusOne = {
    type: "MINUS_ONE_TO_COUNT"
}

const getUser = {
    type: "FETCH_USER_STARTED"
}

export {
    addOne,
    minusOne,
    getUser
}