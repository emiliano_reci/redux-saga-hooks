const defaultState = {
    count: 0
};

const reducer = (state = defaultState, action) => {
    switch (action.type) {
        //ejemplo 1
        case 'ADD_ONE_TO_COUNT':
            return {
                ...state,
                count: state.count + 1
            }
        case 'MINUS_ONE_TO_COUNT':
            return {
                ...state,
                count: state.count - 1
            }
        //ejemplo 2    
        case 'FETCH_USER_STARTED':
            return {
                ...state,
                loading: true
            }
        case 'FETCH_USER_SUCCESS':
            return {
                ...state,
                loading: false,
                news: action.payload
            }
        case 'FETCH_USER_FAILED':
            return {
                ...state,
                loading: false,
                errorFetch: action.payload
            }
        default:
            return state;
    }
}

export default reducer;