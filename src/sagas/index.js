import { put, takeLatest, all, call } from 'redux-saga/effects';
import { fetchData } from '../services';

function* fetchUser() {
  try{
    const data = yield call(fetchData);
    yield put({
      type: 'FETCH_USER_SUCCESS',
      payload: data
    }) 
  }catch(err){
    yield put({
      type: 'FETCH_USER_FAILED',
      payload: err
    })
  }
}

//es un escuchador de acciones
function* actionWatcher() {
  yield takeLatest('FETCH_USER_STARTED', fetchUser)
}


export default function* rootSaga() {
  yield all([
    actionWatcher()
  ]);
}







 
//const URL_API = 'https://newsapi.org/v1/articles?source=cnn&apiKey=c39a26d9c12f48dba2a5c00e35684ecc';
//const llave = '8KNNNz88WAG0rOk7aYb3DmUopCVAI9wJ'
//const URL_NY =`https://api.nytimes.com/svc/movies/v2/reviews/search.json?query=godfather&api-key=${llave}`
