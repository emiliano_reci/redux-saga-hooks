const URL_API_NEWS = 'https://newsapi.org/v1/articles?source=cnn&apiKey=c39a26d9c12f48dba2a5c00e35684ecc';

const fetchData = async () => {
    const response = await fetch(URL_API_NEWS);
    const data = await response.json();
    return data.articles;
}

export {
    fetchData
}